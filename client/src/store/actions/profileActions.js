export const ACTION_PROFILE_SET = 'profile:SET'
export const ACTION_PROFILE_LOGOUT = 'profile:LOGOUT'
export const ACTION_PROFILE_ERROR = 'profile:ERROR'
export const ACTION_PROFILE_UPDATE = 'profile:UPDATE'
export const ACTION_PROFILE_TRANSLATIONS_DELETE = 'profile:TRANSLATIONS_DELETE'
//Fetch profile for translation maybe?

export const profileSetAction = (payload) => ({
    type: ACTION_PROFILE_SET,
    payload
})

export const profileLogoutAction = () => ({
    type: ACTION_PROFILE_LOGOUT

})

export const profileErrorAction = (payload) => ({
    type: ACTION_PROFILE_ERROR,
    payload
})

export const profileUpdateAction = (payload) => ({
    type: ACTION_PROFILE_UPDATE,
    payload
})

export const profileTranslationsDeleteAction = (payload) => ({
    type: ACTION_PROFILE_TRANSLATIONS_DELETE,
    payload
})
