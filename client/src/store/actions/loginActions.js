export const ACTION_LOGIN = 'login:LOGIN'
export const ACTION_LOGIN_CREATE = 'login:CREATE' // BLæææ
export const ACTION_LOGIN_AUTHENTICATE = 'login:AUTHENTICATE'
export const ACTION_LOGIN_ERROR = 'login:ERROR'

export const loginAction = (payload) => ({
    type: ACTION_LOGIN,
    payload
})

export const loginCreateAction = (payload) => ({
    type: ACTION_LOGIN_CREATE,
    payload
})

export const loginAuthenticateAction = (payload) => ({
    type: ACTION_LOGIN_AUTHENTICATE,
    payload
})

export const loginErrorAction = (payload) => ({
    type: ACTION_LOGIN_ERROR,
    payload
})