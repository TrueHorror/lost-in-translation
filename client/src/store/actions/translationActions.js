export const ACTION_TRANSLATION_FETCH = 'translation:FETCH'
export const ACTION_TRANSLATION_PUT = 'translation:PUT'

export const translationFetchAction = () => ({
    type: ACTION_TRANSLATION_FETCH
})

export const translationPutAction = (payload) => ({
    type: ACTION_TRANSLATION_PUT,
    payload
})