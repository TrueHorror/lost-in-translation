import { combineReducers } from "redux";

import { loginReducer } from "./loginReducer";
import { translationReducer } from "./translationReducer";
import { profileReducer } from "./profileReducer";

export const appReducer = combineReducers({
    loginReducer,
    profileReducer,
    translationReducer
})