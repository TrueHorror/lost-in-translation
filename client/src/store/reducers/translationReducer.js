import { 
    ACTION_TRANSLATION_FETCH,
    ACTION_TRANSLATION_PUT 
} from '../actions/translationActions'

const initialState = {
    translations: [],
    fetching: false,
    error: null
}

export function translationReducer(state = initialState, action) {

    switch( action.type ) {

        case ACTION_TRANSLATION_FETCH:
            return {
                fetching: true,
                error: null
            }

        case ACTION_TRANSLATION_PUT:
        return {
            ...state,
            translations: action.payload.translation,
            fetching: true,
            error: null
        }

        default:
            return state
    }

}
