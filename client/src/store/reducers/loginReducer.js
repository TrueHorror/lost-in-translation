import { 
    ACTION_LOGIN,
    ACTION_LOGIN_AUTHENTICATE,
    ACTION_LOGIN_ERROR
} from '../actions/loginActions'

const initialState = {
    username: '',
    fetching: false,
    error: null
}

export function loginReducer(state = initialState, action) {

    switch( action.type ) {

        case ACTION_LOGIN: 
            return {
                ...state,
                username: action.payload,
                fetching: true
            }

        case ACTION_LOGIN_AUTHENTICATE:
            return {
                ...state,
                username: action.payload,
                fetching: true,
                error: null
            }
        
        case ACTION_LOGIN_ERROR:
            return {
                ...state,
                error: action.payload
            }

        default:
            return state
    }

}
