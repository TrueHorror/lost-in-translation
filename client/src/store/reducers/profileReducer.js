import { 
    ACTION_PROFILE_SET,
    ACTION_PROFILE_LOGOUT,
    ACTION_PROFILE_ERROR,
    ACTION_PROFILE_UPDATE,
    ACTION_PROFILE_TRANSLATIONS_DELETE
} from '../actions/profileActions'

const initialState = {
    username: null,
    translations: [],
    id: null,
    error: null

}

export function profileReducer(state = initialState, action) {

    switch( action.type ) {

        case ACTION_PROFILE_SET: 
            return {
                ...state,
                ...action.payload
                
            }

        case ACTION_PROFILE_LOGOUT:
            return initialState
        
        case ACTION_PROFILE_ERROR:
            return {
                ...state,
                error: action.payload
            }
        case ACTION_PROFILE_UPDATE:
            return {
                ...state,
                translations: [...state.translations, action.payload.translation]
            }

        case ACTION_PROFILE_TRANSLATIONS_DELETE:
            return {
                ...state,
                translations: []
            }

        default:
            return state
    }

}
