import { applyMiddleware } from 'redux';
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { appReducer } from './reducers/'
import { loginMiddleware } from './middleware/loginMiddleware'
import { profileMiddleware} from './middleware/profileMiddleware'
import { translationMiddleware } from './middleware/translationMiddleware'




export const store = createStore(
    appReducer,
    composeWithDevTools(
        applyMiddleware( loginMiddleware, profileMiddleware, translationMiddleware )
    )
)
