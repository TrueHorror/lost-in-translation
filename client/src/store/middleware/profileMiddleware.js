import { 
    ACTION_PROFILE_SET,
    ACTION_PROFILE_LOGOUT,
    ACTION_PROFILE_UPDATE,
    ACTION_PROFILE_TRANSLATIONS_DELETE
} from '../actions/profileActions'
import { addTranslationToUser, deleteTranslations } from "../../components/translate/TranslateApi";
import { fetchUser } from '../../components/login/LoginApi';
import { setStorage, removeStorage } from '../../utils/storage';

const profileStorageKey = '_lit-p' 
export const profileMiddleware = () => next => async action =>{
    
    if (action.type === ACTION_PROFILE_SET) {
        setStorage(profileStorageKey, action.payload);
        
    }
    if (action.type === ACTION_PROFILE_LOGOUT) {
        removeStorage(profileStorageKey)
    }
    if (action.type === ACTION_PROFILE_UPDATE) {
        console.log(action.payload.translation);
        console.log(action.payload.id);
        
        try {
            await fetchUser(action.payload.id)
            .then( async (r) => {
                console.log(r);
                await addTranslationToUser(
                    [...r.translations, action.payload.translation],
                    r.id
                );
            })
            
        } catch (e) {
            console.log(e.message);
        }


        
        
    }
    if (action.type === ACTION_PROFILE_TRANSLATIONS_DELETE) {
        try {
            console.log(action.payload);
            console.log("deleting database translations");
            await deleteTranslations(action.payload)
        } catch (e) {
            console.log(e.message);
        }
    }

    return next(action)
}