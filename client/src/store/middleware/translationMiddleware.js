import {
  ACTION_TRANSLATION_PUT
} from "../actions/translationActions";

import {profileUpdateAction} from '../actions/profileActions';



export const translationMiddleware = ({ dispatch}) => (next) => async (action) => {
   
  next(action);
  if (action.type === ACTION_TRANSLATION_PUT) {
    console.log(action.payload);
    dispatch(profileUpdateAction(action.payload));
    
  }

};
