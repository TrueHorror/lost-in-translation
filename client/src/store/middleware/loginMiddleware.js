import { ACTION_LOGIN, ACTION_LOGIN_CREATE, ACTION_LOGIN_AUTHENTICATE} from '../actions/loginActions'
import { fetchUser, createUser } from '../../components/login/LoginApi';
import {loginCreateAction, loginAuthenticateAction, loginErrorAction } from '../actions/loginActions'
import {profileSetAction} from '../actions/profileActions'

export const loginMiddleware = ({ dispatch }) => next => async action => {

    next( action )

    if (action.type === ACTION_LOGIN_CREATE) {
        try {
            const newUser = await createUser({
                username: action.payload,
                translations: []
            })
            dispatch(profileSetAction(newUser))
        }
        catch (error) {
            dispatch( loginErrorAction( error.message ) )
        }
    }
    if (action.type === ACTION_LOGIN){
       dispatch(loginAuthenticateAction(action.payload))
    
    }
    if (action.type === ACTION_LOGIN_AUTHENTICATE) {
        console.log("Authenticating " + action.payload);
        try {
            const existingUser = await fetchUser(action.payload)
            console.log(existingUser);
            if (existingUser) {
                let userWithSlicedTranslations = {
                    ...existingUser,
                    translations: existingUser.translations.reverse().slice(0,10)
                        
                }
                dispatch(profileSetAction(userWithSlicedTranslations))
            } 
            else {
                dispatch(loginCreateAction(action.payload))
            }
        } catch (error) {
            dispatch( loginErrorAction( error.message ) )
        }
        
        
    }
    
}
