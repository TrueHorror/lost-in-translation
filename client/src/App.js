import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";

import Home from "./pages/Home";
import Translate from "./pages/Translate";
import Profile from "./pages/Profile";
import NotFound from "./components/notFound/NotFound";

import Header from "./components/hoc/Header";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Header />
          <Home />
        </Route>
        <Route path="/translate">
          <Header />

          <Translate />
        </Route>
        <Route path="/profile/:id">
          <Header />

          <Profile />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
