import LoginForm from '../components/login/LoginForm'
import Main from '../components/hoc/Main'


function Home(){
  
    return (

        <Main>
          <LoginForm />
        </Main>
    )
}

export default Home