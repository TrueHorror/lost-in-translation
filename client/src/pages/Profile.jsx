import ProfileTranslationHistory from '../components/profile/ProfileTranslationHistory'
import ProfileLogoutButton from '../components/profile/ProfileLogoutButton'
import ProfileBackToTranslateButton from '../components/profile/ProfileBackToTranslateButton'
import ProfileClearTranslationsButton from '../components/profile/ProfileClearTranslationsButton'

import Main from '../components/hoc/Main'
import { useEffect } from 'react'
import {useHistory} from 'react-router'

function Profile(){
  const history = useHistory();
  useEffect(() => {
    if (!localStorage.getItem("_lit-p")) {
      history.push("/");
    }
  }, []);
  
    return (
      <Main>
        <ProfileLogoutButton/>
        <ProfileBackToTranslateButton/>
        <ProfileClearTranslationsButton/>
        <ProfileTranslationHistory />
      </Main>
    )
}

export default Profile