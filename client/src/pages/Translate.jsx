import TranslateInput from '../components/translate/TranslateInput'
import Main from '../components/hoc/Main'
import {useHistory} from 'react-router'
import {useEffect} from 'react'

function Translate(){

  const history = useHistory();
  useEffect(() => {
    if (!localStorage.getItem("_lit-p")) {
      history.push("/");
    }
  }, []);
    
    return (
        <Main>
          <TranslateInput />
        </Main>
    )
}

export default Translate