import {createContext, useReducer} from 'react'

export const AppContext = createContext()

export const ACTION_SET_LOGIN = 'username:LOGIN'


function appReducer(state, action){
    switch(action.type){
        case ACTION_SET_LOGIN:
            return {
                username: action.payload
            }
            default:
                return state
    }
}

const initialState = {
    username: null
}

export function AppProvider(props){
    
    const [state, dispatch] = useReducer(appReducer, initialState);

    // const actions = {
    //     async fetchUsersAction() {
    //         const users = await fetchUsers();

    //         dispatch({type:})
    //     }
    // }



    return (
        <AppContext.Provider value={[state, dispatch]}>
            {props.children}
        </AppContext.Provider>
    )
}