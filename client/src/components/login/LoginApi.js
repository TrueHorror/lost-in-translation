const BASE_URL = 'http://localhost:5000'

export function fetchUser(username){
    return fetch(`${BASE_URL}/users`)
                .then(r => r.json())
                .then(users => {
                    return users.find(user => user.username === username);
                })
}

export function fetchUsers(){
    return fetch(`${BASE_URL}/users`).then(res => res.json());
}

export function createUser(payload){
    console.log(payload);
    return fetch(`${BASE_URL}/users`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: payload.username,
            translations: payload.translations 
        })
    })
    .then(r => r.json())
    //.then()// only 10 translations (slice)
    
}