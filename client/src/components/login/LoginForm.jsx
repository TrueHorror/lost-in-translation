import {useState, useEffect} from 'react'
import styles from './LoginForm.module.css'
import { useHistory } from "react-router-dom"; // need?

import {useDispatch, useSelector} from 'react-redux'
import {loginAction} from '../../store/actions/loginActions'

function LoginForm() {

    const history = useHistory()

    const dispatch = useDispatch()
    const {username} = useSelector(state => state.profileReducer)
    
    const [loginInput, setLoginInput] = useState('')
    const [inputError, setInputError] = useState({})
    const [inputPlaceholder, setInputPlaceholder] = useState('What is your name?')

    useEffect(() => {
        const user = localStorage.getItem('_lit-p')
        if(user){
            history.push('/translate')
            
        }
        
    }, [])

    useEffect(() => {
        if (username) {
            history.push('/translate')
        }
        
    }, [username])

    const capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }

    const handleLoginInputChange = (e) => {
        setInputError({})
        setLoginInput(e.target.value)
        setInputPlaceholder('What is your name?')
    }

    const handleLoginButtonClick = async (e) => {
        e.preventDefault()
        if (loginInput.length === 0) {
            setInputError({
                border: '1px solid red',
                
            })
            setInputPlaceholder(`You have a name, don't you?`)
            return
        }
        setInputError({})
        
        dispatch(loginAction(capitalizeFirstLetter(loginInput.trim()))) // Loging in and setting profile state with login input
        
        setLoginInput('');
    }

    return(
            <form className={styles.LoginForm} action="">
                <div style={inputError} className={styles.LoginInputContainer}>
                <img className={styles.LogoHelloImage} src={require('../../assets/Logo.png').default} alt="logo"/>
                    <input  value={loginInput} onChange={handleLoginInputChange} placeholder={inputPlaceholder} maxLength="25" className={styles.LoginInput} type="text"/>
                    <button className={styles.LoginFormButton} onClick={handleLoginButtonClick}>Login</button>
                </div>     
            </form>
    )
}

export default LoginForm