import HeaderTitle from '../header/HeaderTitle';
import UserAvatar from '../profile/UserAvatar';


import styles from './Header.module.css'

function Header(){
    return (
        <header className={styles.Header}>
            <div className={styles.HeaderContent}>
                <img className={styles.LogoImage} src={require('../../assets/Logo-Hello.png').default} alt="logo-hello"/>
                <HeaderTitle styles={styles}/>
                <UserAvatar/>
            </div>
        </header>
        
    )
}

export default Header