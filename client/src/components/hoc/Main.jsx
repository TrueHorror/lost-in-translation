import styles from "./Main.module.css";
import {useEffect} from 'react'
import {loginAction} from '../../store/actions/loginActions'
import {useDispatch} from 'react-redux'
import {getStorage} from '../../utils/storage'

function Main(props) {

  const dispatch = useDispatch();

  useEffect(() => {
    const user = getStorage("_lit-p");
    if (user) {
      dispatch(loginAction(user.username));
    }
  }, []);

  return <main className={styles.Main}>{props.children}</main>;
}

export default Main;
