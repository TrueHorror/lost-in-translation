import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import { profileTranslationsDeleteAction } from '../../store/actions/profileActions'

function ProfileClearTranslationsButton() {

    const {id} = useSelector(state => state.profileReducer)

    const dispatch = useDispatch()

    const handleClearTranslationsButton = () => {
        console.log(id);
        dispatch(profileTranslationsDeleteAction(id))
        console.log("Delete all translations");
    }

    return (
        <div>
            <button onClick={handleClearTranslationsButton}>Delete all Translations</button>
        </div>
    )
}

export default ProfileClearTranslationsButton
