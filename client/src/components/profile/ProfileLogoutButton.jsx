import { useHistory } from "react-router";
import { useDispatch } from "react-redux";
import {profileLogoutAction} from '../../store/actions/profileActions'

function ProfileLogoutButton() {

    const dispatch = useDispatch()
    const history = useHistory()

    const handleLogoutButtonClick = () => {

        dispatch(profileLogoutAction())
        history.push('/')
        console.log("logout");
    }


    return (
        <div>
            <button onClick={handleLogoutButtonClick}>Logout</button>
        </div>
    )
}

export default ProfileLogoutButton
