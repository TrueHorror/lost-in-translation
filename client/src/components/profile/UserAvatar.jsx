import styles from "./Profile.module.css";
import { useSelector } from "react-redux";
import {useHistory} from 'react-router-dom'

function UserAvatar() {
    const { username } = useSelector((state) => state.profileReducer);

    const history = useHistory()

    const handleProfileAvatarClick = (e) => {
      history.push(`/profile/${username}`)
    }

  return (
    <section className={styles.UserSection}>
      <p className={styles.ProfileNameInHeader} onClick={handleProfileAvatarClick}>{username}</p>
    </section>
  );
}

export default UserAvatar;
