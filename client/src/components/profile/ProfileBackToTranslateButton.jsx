import { useHistory } from "react-router";


function ProfileBackToTranslateButton() {

    const history = useHistory()

    const handleBackToTranslation = () => {
        history.push('/translate')
        console.log("Back to translate");
    }

    return (
        <div>
            <button onClick={handleBackToTranslation}>Translation</button>
        </div>
    )
}
export default ProfileBackToTranslateButton