import {useSelector} from 'react-redux'
import {useEffect} from 'react'
function ProfileTranslationHistory(){

    const {translations} = useSelector(state => state.profileReducer)

    
    useEffect(() => {
        console.log("rendering translation history");
    }, [])


    return (

        
        <div className="historyList">
            <ul>
                {translations.map((t, index) => (
                     t[0] //deleted property from translations for displaying purposes
                        ?   null

                        :   <li>{t.key}</li>
                                
                          
                       
                ))}
            </ul>
        </div>
    )
}

export default ProfileTranslationHistory