import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  translationPutAction,
} from "../../store/actions/translationActions";

import Sign from "./Sign";
import styles from "./Translate.module.css";

function TranslateInput() {
  const dispatch = useDispatch();

  const { username } = useSelector((state) => state.profileReducer);

  const [translateInput, setTranslateInput] = useState("");
  const [letters, setLetters] = useState([]);

  

  useEffect(() => {
    if (letters.length === 0) {
      return;
    }
    const trimmedWord = translateInput.trim();

    const translation = {
      deleted: false,
      key: trimmedWord,
      asl: letters,
    };

    dispatch(
      translationPutAction({
        translation: translation,
        id: username,
      })
    );


    setTranslateInput("");
  }, [letters]);

  const handleTranslateInputChange = (e) => {
    setTranslateInput(e.target.value);
  };

  const handleTranslateButtonClick = (e) => {
    e.preventDefault();
    //Need error message for cannot be only space

    const lettersFromInput = translateInput.toLowerCase().trim().split("");
    setLetters(lettersFromInput);

    //Set translation for profile
  };

  return (
    <>
      <form action="">
        <div className={styles.TranslateInputContainer}>
          <input
            maxLength="40"
            value={translateInput}
            onChange={handleTranslateInputChange}
            placeholder="Type something to translate"
            className={styles.TranslateInput}
            type="text"
          />
          <button
            className={styles.TranslateButton}
            onClick={handleTranslateButtonClick}
          >
            Translate
          </button>
        </div>
      </form>
      <div className={styles.TranslateOutputContainer}>
        {letters.map((letter, index) => (
          <>
            {letter && letter === " " ? (
              <div key={index} className={styles.TranslateSpace}>
                
              </div>
              
            ) : (
              <>
                <Sign key={index} letter={letter} />
                <p>{letter}</p>
              </>
            )}
          </>
        ))}
      </div>
    </>
  );
}

export default TranslateInput;
