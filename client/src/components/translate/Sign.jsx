function Sign({ letter }) {
    return (
        <div>
            <img src={ require(`../../assets/individial_signs/${letter}.png`).default } alt={ letter } />
        </div>
    )
}
export default Sign