const BASE_URL = "http://localhost:5000";

export function addTranslationToUser( translation, user) {
  const data = {
    translations: translation,
    
  }
  return fetch(`${BASE_URL}/users/${user}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then((r) => r.json());
}

export function getTaranslationFromUser(user) {
  return fetch(`${BASE_URL}/users/${user}`)
              .then(r => r.json())
              .then(user => { 
                return user.taranslations
              })
}

export function deleteTranslations(user){
  const data = {
    translations: [],
  }
  return fetch(`${BASE_URL}/users/${user}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  }).then(r => r.json())
}





