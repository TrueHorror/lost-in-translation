function HeaderTitle(styles){

    return (
        <section className={styles.HeaderSection}>
            <h1>Lost in Translation</h1>
        </section>
    )
}

export default HeaderTitle